#! /usr/bin/env python3

import tkinter as tk
from tkinter import messagebox, Checkbutton, IntVar
import random

class CandleGame(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Candle Game")
        self.geometry("600x400")
        self.candles = 21
        self.player_score = 0
        self.computer_score = 0
        self.sound_enabled = IntVar(value=1)  # Sound is enabled by default
        self.dark_theme = IntVar(value=0)  # Light theme by default
        self.init_ui()
        self.apply_window_constraints()

    def init_ui(self):
        self.configure(bg="light gray")
        self.candles_label = tk.Label(self, text='🕯️' * self.candles, font=('Helvetica', 24), bg="light gray")
        self.candles_label.pack(pady=20)
        
        self.info_label = tk.Label(self, text="Your turn", font=('Helvetica', 14), bg="light gray")
        self.info_label.pack(pady=10)
        
        self.buttons_frame = tk.Frame(self, bg="light gray")
        for i in range(1, 5):
            btn = tk.Button(self.buttons_frame, text=str(i), 
                            command=lambda c=i: self.remove_candles(c, "player"))
            btn.pack(side=tk.LEFT, padx=5)
        self.buttons_frame.pack(pady=10)
        
        self.score_label = tk.Label(self, text="Player: 0 | Computer: 0", font=('Helvetica', 16), bg="light gray")
        self.score_label.pack(pady=10)

        Checkbutton(self, text="Enable Sound", variable=self.sound_enabled, bg="light gray").pack()
        Checkbutton(self, text="Dark Theme", variable=self.dark_theme, command=self.switch_theme, bg="light gray").pack()
        
        control_frame = tk.Frame(self, bg="light gray")
        tk.Button(control_frame, text="New Game", command=self.reset_game).pack(side=tk.LEFT, padx=10)
        tk.Button(control_frame, text="Exit", command=self.destroy).pack(side=tk.RIGHT, padx=10)
        control_frame.pack(side=tk.BOTTOM, pady=10, fill=tk.X, expand=True)

    def remove_candles(self, count, player):
        self.candles -= count
        self.update_display()
        if self.candles <= 1:
            self.game_over(player)
        elif player == "player":
            self.after(500, self.computer_turn)

    def update_display(self):
        self.candles_label.config(text='🕯️' * self.candles)

    def computer_turn(self):
        remove = random.randint(1, min(4, self.candles - 1))
        self.info_label.config(text=f"Computer removed {remove} candles.")
        self.remove_candles(remove, "computer")

    def game_over(self, winner):
        if winner == "player":
            self.player_score += 1
            messagebox.showinfo("Game Over", "You win! The computer took the last candle.")
        else:
            self.computer_score += 1
            messagebox.showinfo("Game Over", "You lose! You took the last candle.")
        self.score_label.config(text=f"Player: {self.player_score} | Computer: {self.computer_score}")
        self.reset_game()

    def reset_game(self):
        self.candles = 21
        self.update_display()
        self.info_label.config(text="Your turn")

    def switch_theme(self):
        if self.dark_theme.get():
            self.configure(bg="gray12")
            self.candles_label.configure(bg="gray12", fg="white")
            self.info_label.configure(bg="gray12", fg="white")
            self.score_label.configure(bg="gray12", fg="white")
            self.buttons_frame.configure(bg="gray12")
            for widget in self.buttons_frame.winfo_children():
                widget.configure(bg="gray12", fg="white")
            self.info_label.config(text="Dark Theme Enabled")
        else:
            self.configure(bg="light gray")
            self.candles_label.configure(bg="light gray", fg="black")
            self.info_label.configure(bg="light gray", fg="black")
            self.score_label.configure(bg="light gray", fg="black")
            self.buttons_frame.configure(bg="light gray")
            for widget in self.buttons_frame.winfo_children():
                widget.configure(bg="light gray", fg="black")
            self.info_label.config(text="Light Theme Enabled")

    def apply_window_constraints(self):
        min_width = int(self.winfo_screenwidth() * 0.2)
        min_height = int(self.winfo_screenheight() * 0.2)
        self.minsize(min_width, min_height)

if __name__ == "__main__":
    app = CandleGame()
    app.mainloop()

